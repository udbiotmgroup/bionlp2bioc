package brat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import annotation.Entity;
import annotation.Event;
import annotation.Relation;
import bionlp.BatchProcessor;
import bionlp.BioNLP2BioC;

public class Brat2BioC extends BioNLP2BioC {

  /**
   * java Brat2BioC [BioNLP corpus directory] [BioC output file]
   * 
   * @param args
   * @throws IOException
   */
  @SuppressWarnings("static-access")
  public static void main(String args[])
      throws IOException {

    // create Options object
    Options options = new Options();
    options.addOption("help", false, "display this help and exit");
    Option input = OptionBuilder.withArgName("dir").hasArg()
        .withDescription("input directory").create('i');
    Option output = OptionBuilder.withArgName("file").hasArg()
        .withDescription("output file").create('o');
    Option key = OptionBuilder.withArgName("string").hasArg()
        .withDescription("key file").create('k');
    Option source = OptionBuilder.withArgName("string").hasArg()
        .withDescription("source").create('s');
    Option help = new Option("help", "print this message");

    options.addOption(input);
    options.addOption(output);
    options.addOption(key);
    options.addOption(source);
    options.addOption(help);

    CommandLineParser parser = new GnuParser();
    CommandLine cmd = null;
    try {
      // parse the command line arguments
      cmd = parser.parse(options, args);
    } catch (ParseException exp) {
      // oops, something went wrong
      System.err.println("Parsing failed.  Reason: " + exp.getMessage());
      printHelp(options);
      System.exit(1);
    }

    if (!cmd.hasOption('i')) {
      printHelp(options);
      System.exit(1);
    }
    if (!cmd.hasOption('o')) {
      printHelp(options);
      System.exit(1);
    }
    if (!cmd.hasOption('k')) {
      printHelp(options);
      System.exit(1);
    }
    if (!cmd.hasOption('s')) {
      printHelp(options);
      System.exit(1);
    }

    File dir = new File(cmd.getOptionValue('i'));
    if (!dir.isDirectory()) {
      printHelp(options);
      System.exit(1);
    }
    File outputFile = new File(cmd.getOptionValue('o'));
    if (outputFile.exists()) {
      System.err.println(outputFile + " exists and will be overwriten.");
    }
    try {
      BatchProcessor b = new Brat2BioC(dir.getAbsolutePath(), outputFile,
          "bioc-isimp-simplification_v2.key", "PubMed");
      b.process();
    } catch (Exception e) {
      printHelp(options);
      System.exit(1);
    }
  }

  private static void printHelp(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp("Brat2BioC [OPTIONS] [INPUT_DIR] [OUTPUT]\n"
        + "Convert Brat corpus into BioC format.", options);
  }

  Brat2BioC(String dir, File output, String keyFilename, String source) {
    super(dir, output, keyFilename, source);
  }

  @Override
  protected void readResource(String basename) {
    try {
      text = readText(dir + "/" + basename + ".txt");
    } catch (IOException e1) {
      e1.printStackTrace();
    }
    entities = new ArrayList<Entity>();
    events = new ArrayList<Event>();
    relations = new ArrayList<Relation>();

    try {
      readA2(dir + "/" + basename + ".ann");
    } catch (NumberFormatException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}
