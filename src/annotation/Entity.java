package annotation;

import java.util.ArrayList;
import java.util.List;


public class Entity {

  public String        id;
  public String        type;
  public List<Integer> froms;
  public List<Integer> tos;
  public String        text;

  public Entity() {
    froms = new ArrayList<Integer>();
    tos = new ArrayList<Integer>();
  }
}