package annotation;

import java.util.ArrayList;
import java.util.List;

public class Event {

  public String       id;
  public String       type;
  public String       triggerId;
  public List<String> argType;
  public List<String> argId;

  public Event() {
    argType = new ArrayList<String>();
    argId = new ArrayList<String>();
  }
}