package annotation;

import java.util.ArrayList;
import java.util.List;


public class Relation {

  public String       id;
  public String       type;
  public List<String> argType;
  public List<String> argId;

  public Relation() {
    argType = new ArrayList<String>();
    argId = new ArrayList<String>();
  }
}