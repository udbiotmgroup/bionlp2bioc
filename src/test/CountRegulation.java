package test;

import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.xml.stream.XMLStreamException;

import bioc.BioCCollection;
import bioc.BioCDocument;
import bioc.BioCNode;
import bioc.BioCRelation;
import bioc.io.BioCCollectionReader;
import bioc.io.BioCFactory;

public class CountRegulation {

  /**
   * @param args
   * @throws XMLStreamException
   * @throws FileNotFoundException
   */
  public static void main(String[] args)
      throws FileNotFoundException, XMLStreamException {
    BioCCollectionReader reader = BioCFactory.newFactory(BioCFactory.STANDARD)
        .createBioCCollectionReader(
            new FileReader("corpus/BioNLP-ST_2011_genia_train_data_rev1.xml"));
    BioCCollection collection = reader.readCollection();

    int num[][] = new int[8][4];

    for (BioCDocument doc : collection) {
      for (BioCRelation rel : doc.getRelations()) {
        String type = rel.getInfon("event type");
        if (type == null) {
          continue;
        }
        int i = 0;
        if (type.equals("Regulation")) {
          i = 1;
        } else if (type.equals("Positive_regulation")) {
          i = 2;
        } else if (type.equals("Negative_regulation")) {
          i = 3;
        } else {
          continue;
        }
        boolean causeE = false;
        boolean causeP = false;
        boolean themeE = false;
        boolean themeP = false;
        for (BioCNode node : rel.getNodes()) {
          if (node.getRole().equals("Cause")) {
            if (node.getRefid().startsWith("E")) {
              causeE = true;
            } else if (node.getRefid().startsWith("T")) {
              causeP = true;
            } else {
              System.err.println("error for Cause");
            }
          } else if (node.getRole().equals("Theme")) {
            if (node.getRefid().startsWith("E")) {
              themeE = true;
            } else if (node.getRefid().startsWith("T")) {
              themeP = true;
            } else {
              System.err.println("error for Theme");
            }
          }
        }
        if (causeE) {
          if (themeE) {
            num[0][i]++;
          } else if (themeP) {
            num[1][i]++;
          } else {
            num[2][i]++;
          }
        } else if (causeP) {
          if (themeE) {
            num[3][i]++;
          } else if (themeP) {
            num[4][i]++;
          } else {
            num[5][i]++;
          }
        } else {
          if (themeE) {
            num[6][i]++;
          } else if (themeP) {
            num[7][i]++;
          } else {
            System.err.println("error: no theme, no cause");
          }
        }
      }
    }

    String title[] = { "event-event", "event-protein", "event-null",
        "protein-event", "protein-protein", "protein-null", "null-event",
        "null-protein" };
    System.out.println("Cause-Theme\tAll\tReg\tPos_reg\tNeg_reg");
    System.out.println("-----------------------------------------------");
    for (int i = 0; i < num.length; i++) {
      for (int j = 1; j < num[i].length; j++) {
        num[i][0] += num[i][j];
      }
      System.out.print(title[i]);
      for (int j = 0; j < num[i].length; j++) {
        System.out.print("\t" + num[i][j]);
      }
      System.out.println();
    }
  }
}
