package bionlp;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;

/**
 * 
 * If path is a txt file, then process the file. If path is a directory, then
 * process all txt files. Otherwise throw Exception
 * 
 * @author Yifan Peng
 * 
 */
public abstract class BatchProcessor {

  protected String                path;
  protected File                  dir;
  protected List<String>          basenames;
  protected boolean               debug;

  private final static FileFilter filter = FileFilterUtils
                                             .suffixFileFilter(".txt");

  public BatchProcessor(File path) {
    this(path.getAbsolutePath());
  }

  public BatchProcessor(String path) {
    this.path = path;
    debug = false;

    dir = new File(path);
    if (!dir.exists()) {
      System.err.println("Cannot find file/dir: " + dir);
      System.exit(1);
    } else if (dir.isFile()) {
      // check if it is a [.txt] file
      if (filter.accept(dir)) {
        String basename = FilenameUtils.getBaseName(dir.getAbsolutePath());
        basenames = Collections.singletonList(basename);
        dir = new File(FilenameUtils.getFullPath(dir.getAbsolutePath()));
      } else {
        throw new IllegalArgumentException("Not a [.txt] file: " + path);
      }
    } else {
      // filter [.txt]
      String[] txtfilenames = dir
          .list(FileFilterUtils.suffixFileFilter(".txt"));
      Arrays.sort(txtfilenames);
      basenames = new ArrayList<String>();
      for (String f : txtfilenames) {
        basenames.add(FilenameUtils.getBaseName(f));
      }
    }
  }

  /**
   * If path is a txt file, then process the file. If path is a directory, then
   * process all txt files. Otherwise throw Exception
   * 
   * @param path
   */
  public final void process() {
    preprocess();
    for (String basename : basenames) {
      readResource(basename);
      processFile(basename);
    }
    postprocess();
  }

  public void setDebug(boolean debug) {
    this.debug = debug;
  }

  protected abstract void preprocess();

  protected abstract void postprocess();

  protected abstract void processFile(String basename);

  protected abstract void readResource(String basename);
}
