package bionlp;

import annotation.Entity;
import annotation.Event;
import annotation.Relation;

public class AnnWriter {

  public static String writeEntity(Entity entity) {
    StringBuilder sb = new StringBuilder();
    sb.append(entity.id).append('\t').append(entity.type).append(' ');
    for (int i = 0; i < entity.froms.size(); i++) {
      if (i != 0) {
        sb.append(';');
      }
      sb.append(entity.froms.get(i)).append(' ').append(entity.tos.get(i));
    }
    sb.append('\t').append(entity.text);
    return sb.toString();
  }

  public static String writeEvent(Event event) {
    StringBuilder sb = new StringBuilder();
    sb.append(event.id).append('\t').append(event.type).append(':')
        .append(event.triggerId);
    for (int i = 0; i < event.argType.size(); i++) {
      sb.append(' ').append(event.argType.get(i)).append(':')
          .append(event.argId.get(i));
    }
    return sb.toString();
  }

  public static String writeRelation(Relation relation) {
    StringBuilder sb = new StringBuilder();
    sb.append(relation.id).append('\t').append(relation.type);
    for (int i = 0; i < relation.argType.size(); i++) {
      sb.append(' ').append(relation.argType.get(i)).append(':')
          .append(relation.argId.get(i));
    }
    return sb.toString();
  }
}
