package bionlp;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;

import annotation.Entity;
import annotation.Event;
import annotation.Relation;
import bioc.BioCAnnotation;
import bioc.BioCCollection;
import bioc.BioCDocument;
import bioc.BioCLocation;
import bioc.BioCPassage;
import bioc.BioCRelation;
import bioc.io.BioCCollectionWriter;
import bioc.io.BioCFactory;
import brat.Brat2BioC;

public class BioNLP2BioC extends BatchProcessor {

  /**
   * java BioNLP2BioC [BioNLP corpus directory] [BioC output file]
   * 
   * @param args
   * @throws IOException
   * @throws XMLStreamException
   */
  @SuppressWarnings("static-access")
  public static void main(String args[])
      throws IOException, XMLStreamException {

    // create Options object
    Options options = new Options();
    options.addOption("help", false, "display this help and exit");
    Option input = OptionBuilder.withArgName("dir").hasArg()
        .withDescription("input directory").create('i');
    Option output = OptionBuilder.withArgName("file").hasArg()
        .withDescription("output file").create('o');
    Option help = new Option("help", "print this message");

    options.addOption(input);
    options.addOption(output);
    options.addOption(help);

    CommandLineParser parser = new GnuParser();
    CommandLine cmd = null;
    try {
      // parse the command line arguments
      cmd = parser.parse(options, args);
    } catch (ParseException exp) {
      // oops, something went wrong
      System.err.println("Parsing failed.  Reason: " + exp.getMessage());
      printHelp(options);
      System.exit(1);
    }

    if (!cmd.hasOption('i')) {
      printHelp(options);
      System.exit(1);
    }
    if (!cmd.hasOption('o')) {
      printHelp(options);
      System.exit(1);
    }

    File dir = new File(cmd.getOptionValue('i'));
    if (!dir.isDirectory()) {
      printHelp(options);
      System.exit(1);
    }
    File outputFile = new File(cmd.getOptionValue('o'));
    if (outputFile.exists()) {
      System.err.println(outputFile + " exists and will be overwriten.");
    }
    try {
      BatchProcessor b = new BioNLP2BioC(dir.getAbsolutePath(), outputFile,
          "bionlp-st-2011.key", "bionlp-st-2011");
      b.process();
    } catch (Exception e) {
      printHelp(options);
      System.exit(1);
    }
  }

  private static void printHelp(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp(
        "Brat2BioC [BioNLP corpus directory] [BioC output file]\n"
            + "Convert BioNLP corpus into BioC format.",
        options);
  }

  protected File           output;
  protected BioCCollection collection;
  protected String         keyFilename;
  protected String         source;

  protected String         text;
  protected List<Entity>   entities;
  protected List<Event>    events;
  protected List<Relation> relations;

  public BioNLP2BioC(String dir, File output, String keyFilename, String source) {
    super(dir);
    this.output = output;
    this.keyFilename = keyFilename;
    this.source = source;
  }

  @Override
  protected void preprocess() {
    collection = new BioCCollection();
    Date date = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    collection.setDate(sdf.format(date));
    collection.setSource(source);
    collection.setKey(keyFilename);
  }

  @Override
  protected void postprocess() {
    // print
    try {
      BioCCollectionWriter writer = BioCFactory
          .newFactory(BioCFactory.STANDARD).createBioCCollectionWriter(
              new FileWriter(output));
      writer.writeCollection(collection);
      writer.close();
    } catch (XMLStreamException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void processFile(String basename) {
    System.out.println(basename);

    // document
    BioCDocument doc = new BioCDocument();
    collection.addDocument(doc);
    doc.setID(basename);
    // passage
    BioCPassage passage = new BioCPassage();
    doc.addPassage(passage);

    passage.setOffset(0);
    passage.setText(text);

    // annotation
    for (Entity entity : entities) {
      BioCAnnotation ann = new BioCAnnotation();
      ann.setID(entity.id);
      ann.setText(entity.text);
      for (int i = 0; i < entity.froms.size(); i++) {
        BioCLocation loc = new BioCLocation(entity.froms.get(i),
            entity.tos.get(i) - entity.froms.get(i));
        ann.addLocation(loc);
      }
      Map<String, String> infons = new HashMap<String, String>();
      infons.put("type", entity.type);
      ann.setInfons(infons);
      passage.addAnnotation(ann);
      // assert
      if (entity.froms.size() == 1) {
        String subtext = passage.getText().substring(
            entity.froms.get(0) - passage.getOffset(),
            entity.tos.get(0) - passage.getOffset());
        Validate.isTrue(
            subtext.equals(entity.text),
            "not equal:%s\n%s\n",
            entity.text,
            subtext);
      }
    }

    // event
    for (Event event : events) {
      // rel
      BioCRelation rel = new BioCRelation();
      rel.setID(event.id);
      Map<String, String> infons = new HashMap<String, String>();
      infons.put("type", event.type);
      rel.setInfons(infons);
      // trigger
      rel.addNode(event.triggerId, event.type);
      // arg
      for (int i = 0; i < event.argId.size(); i++) {
        rel.addNode(event.argId.get(i), event.argType.get(i));
      }
      passage.addRelation(rel);
    }

    // relation
    for (Relation relation : relations) {
      // rel
      BioCRelation rel = new BioCRelation();
      rel.setID(relation.id);
      Map<String, String> infons = new HashMap<String, String>();
      infons.put("type", relation.type);
      rel.setInfons(infons);
      // arg
      for (int i = 0; i < relation.argId.size(); i++) {
        rel.addNode(relation.argId.get(i), relation.argType.get(i));
      }
      passage.addRelation(rel);
    }
  }

  @Override
  protected void readResource(String basename) {
    try {
      text = readText(dir + "/" + basename + ".txt");
    } catch (IOException e1) {
      e1.printStackTrace();
    }
    entities = new ArrayList<Entity>();
    events = new ArrayList<Event>();
    relations = new ArrayList<Relation>();

    try {
      readA1(dir + "/" + basename + ".a1");
      readA2(dir + "/" + basename + ".a2");
    } catch (NumberFormatException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  protected String readText(String filename)
      throws IOException {
    return FileUtils.readFileToString(new File(filename));
  }

  protected void readA1(String filename)
      throws NumberFormatException, IOException {
    LineNumberReader reader = new LineNumberReader(new FileReader(filename));
    String line = null;
    while ((line = reader.readLine()) != null) {
      if (line.startsWith("T")) {
        entities.add(AnnReader.readEntity(line));
      } else {
        System.err.println(line);
      }
    }
    reader.close();
  }

  protected void readA2(String filename)
      throws NumberFormatException, IOException {
    LineNumberReader reader = new LineNumberReader(new FileReader(filename));
    String line = null;
    while ((line = reader.readLine()) != null) {
      if (line.startsWith("T")) {
        entities.add(AnnReader.readEntity(line));
      } else if (line.startsWith("E")) {
        events.add(AnnReader.readEvent(line));
      } else if (line.startsWith("M")) {
        events.add(AnnReader.readEventModification(line));
      } else if (line.startsWith("*")) {
        relations.add(AnnReader.readEquivalence(line));
      } else if (line.startsWith("R")) {
        relations.add(AnnReader.readRelation(line));
      } else {
        System.err.println(line);
      }
    }
    reader.close();
  }
}
