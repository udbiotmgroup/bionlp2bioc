# /bin/bash -x

CLASSPATH="bin:lib/*"

#java -cp $CLASSPATH bionlp.BioNLP2BioC -i corpus/2011/BioNLP-ST_2011_genia_devel_data_rev1 -o corpus/BioNLP-ST_2011_genia_devel_data_rev1.xml
#java -cp $CLASSPATH bionlp.BioNLP2BioC -i corpus/2011/BioNLP-ST_2011_genia_train_data_rev1 -o corpus/BioNLP-ST_2011_genia_train_data_rev1.xml
#java -cp $CLASSPATH bionlp.BioNLP2BioC -i corpus/2013/BioNLP-ST-2013_GE_train_data_rev3 -o corpus/BioNLP-ST-2013_GE_train_data_rev3.xml
#java -cp $CLASSPATH bionlp.BioNLP2BioC -i corpus/2013/BioNLP-ST-2013_GE_devel_data_rev3 -o corpus/BioNLP-ST-2013_GE_devel_data_rev3.xml

java -cp $CLASSPATH brat.Brat2BioC -i corpus/isimp -o foo.xml -k bioc-isimp-simplification_v2.key -s PubMed
java -cp $CLASSPATH xml.XmlFormatter foo.xml bioc-isimp-simplification_v2.xml
